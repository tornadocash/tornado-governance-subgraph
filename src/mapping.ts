import { BigInt } from "@graphprotocol/graph-ts";
import {
  Proposal,
  Vote,
  Delegate,
  Undelegate,
  StakeBurn,
  StakeDailyBurn,
} from "../generated/schema";
import {
  ProposalCreated,
  ProposalExecuted,
  Voted,
  Delegated,
  Undelegated,
} from "../generated/Governance/Governance";
import {
  StakeBurned
} from "../generated/RelayerRegistry/RelayerRegistry";

export let ZERO_BI = BigInt.fromI32(0);

export function handleProposalCreated(event: ProposalCreated): void {
  let proposal = Proposal.load(event.params.id.toString());

  if (proposal === null) {
    proposal = new Proposal(event.params.id.toString());
    proposal.timestamp = event.block.timestamp.toI32();
    proposal.blockNumber = event.block.number.toI32();
    proposal.logIndex = event.logIndex.toI32();
    proposal.transactionHash = event.transaction.hash;

    proposal.proposalId = event.params.id.toI32();
    proposal.proposer = event.params.proposer;
    proposal.target = event.params.target;
    proposal.startTime = event.params.startTime.toI32();
    proposal.endTime = event.params.endTime.toI32();
    proposal.description = event.params.description;
    proposal.executed = false;
    proposal.save();
  }
}

export function handleProposalExecuted(event: ProposalExecuted): void {
  let proposal = Proposal.load(event.params.proposalId.toString());

  if (proposal !== null) {
    proposal.executed = true;
    proposal.save();
  }
}

export function handleVoted(event: Voted): void {
  let voted = Vote.load(event.transaction.hash.toHex() + '_' + event.logIndex.toString());

  if (voted === null) {
    voted = new Vote(event.transaction.hash.toHex() + '_' + event.logIndex.toString());
    voted.timestamp = event.block.timestamp.toI32();
    voted.blockNumber = event.block.number.toI32();
    voted.logIndex = event.logIndex.toI32();
    voted.transactionHash = event.transaction.hash;

    voted.proposalId = event.params.proposalId.toI32();
    voted.voter = event.params.voter;
    voted.support = event.params.support;
    voted.votes = event.params.votes;
    voted.from = event.transaction.from;
    voted.input = event.transaction.input;
    voted.save();
  }
}

export function handleDelegated(event: Delegated): void {
  let delegated = Delegate.load(event.transaction.hash.toHex() + '_' + event.logIndex.toString());

  if (delegated === null) {
    delegated = new Delegate(event.transaction.hash.toHex() + '_' + event.logIndex.toString());
    delegated.timestamp = event.block.timestamp.toI32();
    delegated.blockNumber = event.block.number.toI32();
    delegated.logIndex = event.logIndex.toI32();
    delegated.transactionHash = event.transaction.hash;

    delegated.account = event.params.account;
    delegated.delegateTo = event.params.to;
    delegated.save();
  }
}

export function handleUndelegated(event: Undelegated): void {
  let undelegated = Undelegate.load(event.transaction.hash.toHex() + '_' + event.logIndex.toString());

  if (undelegated === null) {
    undelegated = new Undelegate(event.transaction.hash.toHex() + '_' + event.logIndex.toString());
    undelegated.timestamp = event.block.timestamp.toI32();
    undelegated.blockNumber = event.block.number.toI32();
    undelegated.logIndex = event.logIndex.toI32();
    undelegated.transactionHash = event.transaction.hash;

    undelegated.account = event.params.account;
    undelegated.delegateFrom = event.params.from;
    undelegated.save();
  }
}

export function handleStakeBurned(event: StakeBurned): void {
  let stakeBurned = StakeBurn.load(event.transaction.hash.toHex() + '_' + event.logIndex.toString());

  if (stakeBurned === null) {
    stakeBurned = new StakeBurn(event.transaction.hash.toHex() + '_' + event.logIndex.toString());
    stakeBurned.timestamp = event.block.timestamp.toI32();
    stakeBurned.blockNumber = event.block.number.toI32();
    stakeBurned.logIndex = event.logIndex.toI32();
    stakeBurned.transactionHash = event.transaction.hash;

    stakeBurned.relayer = event.params.relayer;
    stakeBurned.amountBurned = event.params.amountBurned;
    stakeBurned.save();

    let timestamp = event.block.timestamp.toI32();
    let dayID = timestamp / 86400;
    let dayStartTimestamp = dayID * 86400;
    
    let dailyBurned = StakeDailyBurn.load(dayStartTimestamp.toString());

    if (dailyBurned === null) {
      dailyBurned = new StakeDailyBurn(dayStartTimestamp.toString());
      dailyBurned.date = dayStartTimestamp;
      dailyBurned.dailyAmountBurned = ZERO_BI;
    }

    dailyBurned.dailyAmountBurned = dailyBurned.dailyAmountBurned.plus(event.params.amountBurned);
    dailyBurned.save();
  }
}